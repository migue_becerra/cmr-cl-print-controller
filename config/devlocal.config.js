'use strict';

module.exports = {
    "secret": "secret-key-local",
    "mongo_database": "local",
    "verify_endpoint": "http://localhost:3001/fif/token/v2/verify/",
    "docgen_generate_endpoint": "http://localhost:3002/api/cmr/docgen/v2/generate",
    "get_request_endpoint": "http://localhost:3005/api/cmr/v1/account/opening/request/",
    "send_mail_endpoint": "http://localhost:3003/fif/email/v1/send",
    "addressee": "PlazaNorteCMR@falabella.cl"
};