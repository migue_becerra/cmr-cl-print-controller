'use strict';

module.exports = {
    "secret": "secret-key-staging",
    "mongo_database": "staging",
    "docgen_generate_endpoint": "http://172.17.0.1:3009/api/cmr/docgen/v2/generate",
    "verify_endpoint": "http://172.17.0.1:3000/fif/token/v2/verify/",
    "get_request_endpoint": "http://172.17.0.1:3005/api/cmr/v1/account/opening/request/",
    "send_mail_endpoint": "http://172.17.0.1:3005/fif/email/v1/send"
};