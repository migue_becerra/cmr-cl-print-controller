'use strict';

module.exports = {
    "secret": "secret-key-local",
    "mongo_database": "local",
    "verify_endpoint": "http://localhost:8009/fif/token/v2/verify/",
    "docgen_generate_endpoint": "http://localhost:8009/api/cmr/docgen/v2/generate",
    "get_request_endpoint": "http://localhost:8009/api/cmr/v1/account/opening/request/",
    "send_mail_endpoint": "http://localhost:8009/fif/email/v1/send",
    "addressee": "PlazaNorteCMR@falabella.cl"
};