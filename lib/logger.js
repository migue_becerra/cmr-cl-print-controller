const fs = require('fs');
const winston = require('winston');
require('winston-daily-rotate-file');
winston.level = process.env.LOG_LEVEL || 'silly';
const logFolder = process.env.LOG_FOLDER || process.env[(process.platform == 'win32') ? 'USERPROFILE' : 'HOME'];
const os = require('os');
const hostName = os.hostname();

let winstonInfo;
let winstonError;

const SERVICE_NAME = 'PRINT.CONTROLLER';
const SERVICE_LOG_NAME = SERVICE_NAME.toLowerCase().replace('.', '-');

const log = (levelIn, moduleName, messageIn) => {
  if (!winstonInfo) {
    winstonInfo = createLogger(levelIn);
  }

  if (!winstonError) {
    winstonError = createLogger(levelIn);
  }
  
  if (levelIn == 'info') {
    winstonInfo.log(levelIn, moduleName, messageIn);
  } else {
    winstonError.log(levelIn, moduleName, messageIn);
  }
}

const createLogger = (levelIn) => {
  let transportsArray = [
    new winston.transports.Console({
      level: levelIn,
      handleExceptions: false,
      json: false,
      colorize: true
    })
  ];
  if (fs.existsSync(logFolder)) {
    transportsArray.push(new(winston.transports.DailyRotateFile)({
      level: levelIn,
      filename: logFolder + '/' + hostName + '-' + SERVICE_LOG_NAME + '-' + levelIn + '-',
      datePattern: 'yyyyMMdd.log',
      handleExceptions: false,
      json: true,
      maxsize: 5242880, //5MB
      maxFiles: 5,
      colorize: false
    }));
  }
  return new winston.Logger({
    transports: transportsArray,
    exitOnError: false
  });
}

const info = (messageIn) => {
  log('info', SERVICE_NAME, messageIn);
}

const error = (messageIn) => {
  log('error', SERVICE_NAME, messageIn);
}

module.exports = {
  level: winston.level,
  info: info,
  error: error,
  folder: logFolder
}
