'use strict';

const chai = require('chai');
const expect = chai.expect;
const httpMocks = require('node-mocks-http');

let req;
let res;

describe('VERIFY.TOKEN.MIDDLEWARE:', () => {
  beforeEach(() => {
    req = httpMocks.createRequest();
    res = httpMocks.createResponse({
      eventEmitter: require('events').EventEmitter
    });
  });

  const executeMiddleware = (next) => {
    const middleware = require('./verify.token.middleware');
    middleware(req, res, next);
  };

  it('Should be rejected if no token passed', (done) => {
    delete req.headers['x-access-token'];
    executeMiddleware(() => {});
    res.on('end', () => {
      expect(res.statusCode).to.be.equal(403);
      done();
    });
  });

  it('Should be verified if token is different than 1737', (done) => {
    req.headers['x-access-token'] = '1234';
    executeMiddleware(() => {});
    res.on('end', () => {
      expect(res.statusCode).to.be.equal(403);
      done();
    });
  });

  it('Should be verified if token is 1737', (done) => {
    req.headers['x-access-token'] = '1737';
    executeMiddleware(() => {
      expect(res.statusCode).to.be.equal(200);
      done();
    });
  });

});
