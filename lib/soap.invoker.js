/*
 * Simple soap invoker 
 */

'use strict';

const config = require('../config');
const lib = require('.');
const logger = lib.logger;
const strongSoap = require('strong-soap');
const soap = strongSoap.soap;

const soapInvoker = (input) => {

  return new Promise((fullfil, reject) => {

    if (!input || !input.wsdl || !input.endpoint) {
        return reject(new Error('Wsdl and Endpoint attributes are required'));
    }

    const hasSoapHeader = input.hasSoapHeader || false;

    soap.createClient(input.wsdl, (errorCliente, client) => {
      if (errorCliente) {
        logger.error('SOAP.INVOKER.CLIENT.ERROR: ' + lib.getStackTrace(errorCliente));
        return reject(errorCliente);
      }
      client.setEndpoint(input.endpoint);

      if (input.clientService) {
        const nameSpaceClientService = '{' + config.client_service_namespace + '}ClientService';
        const qNameClientService = new strongSoap.QName(nameSpaceClientService);
        client.addSoapHeader(input.clientService, qNameClientService);  
      } else if (input.addSoapHeader){
        input.addSoapHeader(client);
      } else if (hasSoapHeader) {
        return reject(new Error('You should define either clientService attribute or ' + 
                                'hasSoapHeader callback when hasSoapHeader is true'));
      }

      logger.info('SOAP.INVOKER: calling ' + input.endpoint);

      const operation = client[input.operationName];
      
      operation(input.body, (errorServicio, result) => {
        if (errorServicio) {
          logger.error('SOAP.INVOKER.OPERATION.ERROR:' + lib.getStackTrace(errorServicio));
          return reject(errorServicio);
        }
        logger.info('SOAP.INVOKER.SUCCESS:  got result successfully');        
        return fullfil(result);
      }, {
        timeout: input.timeout || 30000
      });
    });
  });
};

module.exports = soapInvoker;
