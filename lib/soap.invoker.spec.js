'use strict';

const chai = require('chai');
chai.use(require('chai-as-promised'));
const expect = chai.expect;
const mockery = require('mockery');

describe('SOAP.INVOKER:', () => {

    beforeEach(() => {
        mockery.disable();
        mockery.deregisterAll();
        mockery.enable({
            useCleanCache: true,
            warnOnReplace: false,
            warnOnUnregistered: false
        });
    });

    it('Deberia rechazar peticiones indefinidas', () => {
        return checkRejectedResponse(undefined, 'Wsdl and Endpoint attributes are required');
    });

    const checkRejectedResponse = (input, rejectMessage) => {
        return chai.assert.isRejected(invokeInvoker(input), rejectMessage);
    };

    const invokeInvoker = input => {
        return require('./soap.invoker')(input);
    };

    it('Deberia rechazar peticiones vacias', () => {
        return checkRejectedResponse({}, 'Wsdl and Endpoint attributes are required');
    });

    it('Deberia rechazar peticiones sin wsdl', () => {
        return checkRejectedResponse({ endpoint: 'uri' }, 'Wsdl and Endpoint attributes are required');
    });

    it('Deberia rechazar peticiones sin endpoint', () => {
        return checkRejectedResponse({ wsdl: 'wsdl' }, 'Wsdl and Endpoint attributes are required');
    });

    it('Deberia rechazar cuando existen problemas al crear el cliente', () => {
        mockery.registerMock('strong-soap', {
            soap: {
                createClient: (wsdl, callback) => {
                    return callback(new Error('Error creating client'));
                }
            }
        });
        return checkRejectedResponse({ wsdl: 'wsdl', endpoint: 'uri' }, 'Error creating client');
    });

});