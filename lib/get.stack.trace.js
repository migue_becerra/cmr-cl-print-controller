'use strict';

const getStackTrace = (error) => {
  let stackTrace;
  if (error) {
    stackTrace = error.stack;
  }
  if (!stackTrace || stackTrace === '' || !stackTrace.replace) {
    return '';
  }
  return stackTrace.replace(/\&[^;]+\;/, '');
};

module.exports = getStackTrace;