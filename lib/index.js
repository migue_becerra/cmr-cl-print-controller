const logger = require('./logger');
const getStackTrace = require('./get.stack.trace');

module.exports = {
    logger: logger,
    getStackTrace: getStackTrace
};