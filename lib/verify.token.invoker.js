'use strict';

const lib = require('./index');
const logger = lib.logger;
const request = require('request');
const config = require('../config');

module.exports = (token) => {
  return new Promise((fullfil, reject) => {
    if (!token) {
      logger.info('VERIFY.TOKEN.INVOKER: token is required');
      return reject(new Error('Token is required'))
    }
    logger.info('VERIFY.TOKEN.INVOKER: verify token invoked' + config.verify_endpoint);
    request({
      url: config.verify_endpoint,
      method: 'POST',
      json: {
        token: token
      },
      headers: {
        'content-type': 'application/json'
      }
    }, (err, response, body) => {
      if (err || !body || !response || response.statusCode !== 200) {
        logger.error('VERIFY.TOKEN.INVOKER.ERROR: problem found verifying token' + lib.getStackTrace(err));
        return reject(new Error('Not verified token'));
      } else {
        logger.info('VERIFY.TOKEN.INVOKER: token verified');
        return fullfil(body);
      }
    });
  });
}
