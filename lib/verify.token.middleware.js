'use strict';

const lib = require('./index');
const logger = lib.logger;
const verifyInvoker = require('./verify.token.invoker');

const verify = (req, res, next) => {
  const token = req.body.token || req.query.token || req.headers['x-access-token'];
  verifyInvoker(token).then(body => {
    req.decoded = body.payload;
    next();
  }).catch(err => {
    logger.error('VERIFY.TOKEN.MIDDLEWARE.ERROR: ' + lib.getStackTrace(err));
    return res.status(403).send({
      code: '403',
      message: 'No token provided.'
    });
  });
};

module.exports = verify;
