'use strict';

const chai = require('chai');
chai.use(require('chai-as-promised'));
const expect = chai.expect;

const invoker = require('./verify.token.invoker');

describe('VERIFY.TOKEN.INVOKER:', () => {
    it('Should be rejected if no token passed', () => {
        return chai.assert.isRejected(invoker(), 'Token is required');
    });

    it('Should be verified if token is different than 1737', () => {
        return chai.assert.isRejected(invoker('1234'), 'Not verified token');
    });

    it('Should be verified if token is 1737', () => {
        return expect(invoker('1737')).to.eventually.deep.equal({
            "code": "ValidToken",
            "message": "valid token",
            "payload": {
                "oficinaUltimaConexion": "1737",
                "run": "19",
                "channelHeader": {
                    "commerce": "CMR",
                    "country": "CL",
                    "channel": "Mobile-APP"
                }
            }
        });
    });

});