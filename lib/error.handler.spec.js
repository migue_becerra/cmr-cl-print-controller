'use strict';

const chai = require('chai');
const expect = chai.expect;
const sinon = require('sinon');
const errorHandling = require('./error.handler');

describe('ErrorHandling', () => {
  it('should return status 500', () => {
    const response = {
      status: sinon.spy(),
      send: sinon.spy()
    };
    errorHandling({}, {}, response, {});

    expect(response.status.calledOnce).to.equal(true);
    expect(response.send.calledOnce).to.equal(true);
  });
});
