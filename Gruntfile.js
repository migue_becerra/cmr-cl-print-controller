/*global module:false*/
module.exports = function(grunt) {
  
    grunt.loadNpmTasks('grunt-exec');
    grunt.loadNpmTasks('grunt-stubby');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-watch');
  
  
    grunt.initConfig({
      pkg: grunt.file.readJSON('package.json'),
  
  
      jshint: {
        options: {
          node: true,
          esnext: true,
          bitwise: true,
          camelcase: true,
          curly: true,
          eqeqeq: true,
          immed: true,
          indent: 2,
          latedef: true,
          newcap: true,
          noarg: true,
          quotmark: "single",
          undef: true,
          unused: true,
          strict: false
        },
        lib_test: {
          src: ['./{,!(node_modules)/**/}*.spec.js']
        }
      },
      exec: {
        functionalTest: {
          command: 'npm run functionalTest',
          stdout: true,
          stderr: true
        },
        unitTest: {
          command: 'npm run unitTest',
          stdout: true,
          stderr: true
        }
        /*, start_in_memory_database: {
          command: 'node ./bin/mongo.in.memory.js &',
          stdout: true,
          stderr: true
        }*/
      },
      stubby: {
        stubsServer: {
          options: {
            stubs: 8009,
            tls: 8443,
            admin: 8010
          },
          files: [{
            src: ['stubs/*.yaml']
          }]
        }
      }
    });
  
    grunt.registerTask('functionalTest', ['exec:functionalTest']);
    grunt.registerTask('unitTest', ['exec:unitTest']);
    grunt.registerTask('default', ['stubby', 'unitTest', 'functionalTest']);
  };
  