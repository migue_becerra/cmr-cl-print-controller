'use strict';

const lib = require('../lib/');
const logger = lib.logger;
const request = require('request');
const config = require('../config');

module.exports = (req) => {
  return new Promise((fullfil, reject) => {
    logger.info('GET.REQUEST.INVOKER: invoking document generate ' + config.get_request_endpoint);
    const token = req.body.token || req.query.token || req.headers['x-access-token'];    
    request({
      url: config.get_request_endpoint + req.requestId,
      method: 'GET',
      headers: {
        'x-access-token': token
      }
    }, (err, response, body) => {
      if (err || !body || !response || response.statusCode !== 200) {
        logger.error('GET.REQUEST.INVOKER.ERROR: ' + lib.getStackTrace(err.stack));
        return reject(new Error('Not verified token'));
      } else {
        logger.info('GET.REQUEST.INVOKER: token verified');
        return fullfil({
          prospectRequest: body,
          decoded: req.decoded
        });
      }
    });
  });
}
