'use strict';

const express = require('express');
const allowCrossDomain = require('cors');

module.exports = () => {
    const routes = express.Router();
    routes.use(allowCrossDomain({
        methods: ['GET', 'POST', 'HEAD', 'OPTIONS']
    }));

    routes.get('/health',
        require('./health.controller'));
    routes.get('/auth',
        require('../lib/verify.token.middleware'),
        require('./verify.printing.authorization.middleware'),
        require('./auth.controller'));
    routes.post('/byemail',
        require('../lib/verify.token.middleware'),
        require('./verify.printing.authorization.middleware'),
        require('./request.id.verify.middleware'),
        require('./byemail.validator.middleware'),
        require('./byemail.controller'));

    return routes;
};