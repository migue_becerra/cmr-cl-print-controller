'use strict';

const chai = require('chai');
const httpMocks = require('node-mocks-http');
const EventEmitter = require('events').EventEmitter;

describe('Health controller', () => {
  
  it('Should be ok to get /health', (done) => {
    const req = httpMocks.createRequest();
    const res = httpMocks.createResponse({ eventEmitter: EventEmitter });

    const controller = require('./health.controller');

    res.on('end', () => {
      chai.expect(res.statusCode).to.be.equal(200);
      done();
    });

    controller(req, res);

  });
});
