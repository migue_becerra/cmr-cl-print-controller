'use strict';

const chai = require('chai');
const httpMocks = require('node-mocks-http');
const EventEmitter = require('events').EventEmitter;
const expect = chai.expect;
const mockery = require('mockery');

describe('Byemail controller', () => {
    let req;
    let res;

    beforeEach(() => {
        req = httpMocks.createRequest();
        res = httpMocks.createResponse({ eventEmitter: EventEmitter });

        req.body = {
            documents: ['contrato']
        };

        mockery.disable();
        mockery.deregisterAll();
        mockery.enable({
            useCleanCache: true,
            warnOnReplace: false,
            warnOnUnregistered: false
        });
    });

    const invokeController = (req, res) => {
        return require('./byemail.controller')(req, res);
    };

    const buildFullfilledPromise = req => (
        input => (
            new Promise((fulfill, reject) => (
                fulfill('pulentito')
            ))
        )
    );

    const buildRejectedPromise = req => (
        input => (
            new Promise((fulfill, reject) => (
                reject(new Error('sharsheli'))
            ))
        )
    );

    it('Should be 500 when the document call have errors', (done) => {
        mockery.registerMock('./docgen.invoker', buildRejectedPromise(req));

        res.on('end', () => {
            chai.expect(res.statusCode).to.be.equal(500);
            done();
        });

        invokeController(req, res);
    });


    it('Should be SUCCESS when the document call is OK, and the send email call is OK', (done) => {
        mockery.registerMock('./docgen.invoker', buildFullfilledPromise(req));
        mockery.registerMock('./email.invoker', buildFullfilledPromise(req));

        res.on('end', () => {
            chai.expect(res.statusCode).to.be.equal(200);
            done();
        });

        invokeController(req, res);
    });

    it('Should be 500 when the document call is OK, and the send email call have errors', (done) => {
        mockery.registerMock('./docgen.invoker', buildFullfilledPromise(req));
        mockery.registerMock('./email.invoker', buildRejectedPromise(req));

        res.on('end', () => {
            chai.expect(res.statusCode).to.be.equal(500);
            done();
        });

        invokeController(req, res);
    });
});