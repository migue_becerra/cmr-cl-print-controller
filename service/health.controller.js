'use strict';

const lib = require('../lib');
const COMPONENT_NAME = 'HEALTH.CONTROLLER ';

module.exports = (req, res) => {
  const message = 'Service is up and running';
  lib.logger.info(COMPONENT_NAME + message);
  res.send(message);
};
