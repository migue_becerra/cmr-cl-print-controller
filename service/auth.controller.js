module.exports = (req, res) => {
    res.json({
        code: 'AUTHORIZED',
        message: 'Client authorized'
    });
};