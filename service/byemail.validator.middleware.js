const lib = require('../lib');
const logger = lib.logger;
const _ = require('lodash');

module.exports = (req, res, next) => {
    logger.info('BYEMAIL.VALIDATOR.MIDDLEWARE starting to validate request ');
    if (isNotValid(req.body.documents, req.decoded.requestId)) {
        res.status(400).json({
            code: 'BAD_REQUEST',
            message: 'Not a valid request'
        });
        res.end();
    } else {
        logger.info('BYEMAIL.VALIDATOR.MIDDLEWARE Validated');
        next();
    }
};

const isNotValid = (documents, requestId) => {
    let toReturn = true;
    try {
        validateRequestId(requestId);
        validateDocuments(documents);
        toReturn = false;
    } catch (e) {
        logger.error('BYEMAIL.VALIDATOR.MIDDLEWARE.VALIDATION: ' + e.message);
    }
    return toReturn;
};

const ONLY_NUMBERS = /^\d+$/;
const validateRequestId = (requestId) => {
    if (!requestId || !ONLY_NUMBERS.test(requestId) || requestId.length > 11) {
        throw new Error('RequestId is not permitted (' + requestId + ')');
    }
};

const DOCUMENTS_DOMAIN = ['contrato'];
const validateDocuments = (documents) => {
    if (!_.isArray(documents)) {
        throw new Error('Empty documents are not permitted.');
    }
    _.forEach(documents, (document) => {
        if (!document || !_.includes(DOCUMENTS_DOMAIN, document)) {
            throw new Error('Some documents are not permitted (' + document + ')');
        }
    });
    const duplicates = findDuplicates(documents);
    if (duplicates.length > 0) {
        throw new Error('Duplicates are not permitted.');
    }
};

const findDuplicates = array => (
    _.filter(array, v => _.filter(array, v1 => v1 === v).length > 1)
);