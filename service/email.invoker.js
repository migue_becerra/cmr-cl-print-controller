'use strict';

const lib = require('../lib/');
const logger = lib.logger;
const request = require('request');
const config = require('../config');
const _ = require('lodash');

module.exports = (docgenResponse) => {
    logger.info('EMAIL.INVOKER: starting');

    return new Promise((fullfil, reject) => {
        logger.info('EMAIL.INVOKER: starting2');
        const serviceURL = config.send_mail_endpoint;
        const clientService = docgenResponse.decoded.channelHeader;
        logger.info('EMAIL.INVOKER: invoking ' + serviceURL);

        request({
            method: 'POST',
            url: serviceURL,
            headers: {
                'x-country': clientService.country,
                'x-commerce': clientService.commerce,
                'x-channel': clientService.channel
            },
            json: {
                addressee: config.addressee || 'PlazaNorteCMR@falabella.cl',
                subject: 'Solicitud de apertura',
                bodyHTML: 'Estimados,Por favor imprimir contrato de Apertura,Slds,Ejecutiva de Captación',
                attachment: [{
                    name: 'contrato',
                    pdfBase64: docgenResponse.pdfBase64
                }]
            }
        }, (err, response, body) => {
            if (err || !body || !response || response.statusCode !== 200) {
                logger.error('EMAIL.INVOKER.ERROR: Failed to send email ' + lib.getStackTrace(err));
                return reject(new Error('Failed to send email'));
            } else {
                logger.info('EMAIL.INVOKER: email sent');
                return fullfil(body);
            }
        });
    });
};