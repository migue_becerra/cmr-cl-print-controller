'use strict';

const lib = require('../lib/');
const logger = lib.logger;
const request = require('request');
const config = require('../config');
const _ = require('lodash');

module.exports = (req) => {
    return new Promise((fullfil, reject) => {
        logger.info('DOCGEN.INVOKER: invoking document generate ' + config.docgen_generate_endpoint);
        if (!_.has(req, 'document')) {
            logger.error('DOCGEN.INVOKER.DOCUMENT1: Malformed input');
            return reject(new Error('Malformed input'));
        }
        if (_.isEmpty(translateDocumentToDoctype(req.document))) {
            logger.error('DOCGEN.INVOKER.DOCUMENT2: Unknown document');
            return reject(new Error('Unknown document'));
        }
        const clientService = req.decoded.channelHeader;
        const query = '?requestId=' + req.decoded.requestId + '&doctype=' + translateDocumentToDoctype(req.document);
        request({
            url: config.docgen_generate_endpoint + query,
            method: 'GET',
            headers: {
                'x-country': clientService.country,
                'x-commerce': clientService.commerce,
                'x-channel': clientService.channel
            }
        }, (err, response, body) => {
            if (err || !body || !response || response.statusCode !== 200) {
                logger.error('DOCGEN.INVOKER.ERROR: Failed to generate document ' + lib.getStackTrace(err));
                return reject(new Error('Failed to generate document'));
            } else {
                logger.info('DOCGEN.INVOKER: document generated ');
                const jsonBody = JSON.parse(body);
                jsonBody.document = req.document;
                jsonBody.decoded = req.decoded;
                return fullfil(jsonBody);
            }
        });
    });
};

const DOCUMENTS_MAPPER = {
    "contrato": "GenerarContratoPDF",
    "resumen": "GenerarHojaResumenPDF"
};
const translateDocumentToDoctype = (document) => {
    return DOCUMENTS_MAPPER[document];
};