'use strict';

const lib = require('../lib/index');
const jwt = require('jsonwebtoken');
const _ = require('lodash');
const logger = lib.logger;

const verify = (req, res, next) => {
    logger.info('REQUEST.ID.VERIFY.MIDDLEWARE: Starting Verification');
    if (!_.has(req, 'body.requestId') || !_.has(req, 'decoded.run')) {
        logger.error('REQUEST.ID.VERIFY.MIDDLEWARE: requestId or promotor run is missing');
        res.status(400).json({
            code: 'BAD_REQUEST',
            message: 'requestId or promotor run is missing'
        });
        res.end();
    } else {
        jwt.verify(req.body.requestId, req.decoded.run, (err, decodedRequestId) => {
            if (err) {
                logger.error('REQUEST.ID.VERIFY.MIDDLEWARE: error verifying requestId ' + lib.getStackTrace(err.stack));
                res.status(403).json({
                    code: 'NOT_A_VALID_REQUEST_ID',
                    message: 'error verifying requestId'
                });
                res.end();
            } else {
                logger.info('REQUEST.ID.VERIFY.MIDDLEWARE: requestID verified');
                req.decoded.requestId = decodedRequestId.id;
                next();
            }
        });
    }
};

module.exports = verify;