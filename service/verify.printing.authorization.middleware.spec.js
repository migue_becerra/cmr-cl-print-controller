'use strict';

const chai = require('chai');
const expect = chai.expect;
const httpMocks = require('node-mocks-http');

let req;
let res;

describe('VERIFY.PRINTING.AUTHORIZATION.MIDDLEWARE:', () => {
    beforeEach(() => {
        req = httpMocks.createRequest();
        res = httpMocks.createResponse({
            eventEmitter: require('events').EventEmitter
        });
        req.decoded = {
            oficinaUltimaConexion: '1737'
        };
    });

    const executeMiddleware = (next) => {
        const middleware = require('./verify.printing.authorization.middleware');
        middleware(req, res, next);
    };

    it('Should be bad request if decoded info is not sent', (done) => {
        delete req.decoded;
        res.on('end', () => {
            expect(res.statusCode).to.be.equal(400);
            done();
        });
        executeMiddleware(() => {});
    });

    it('Should be bad request if oficinaUltimaConexion is not sent', (done) => {
        delete req.decoded.oficinaUltimaConexion;
        res.on('end', () => {
            expect(res.statusCode).to.be.equal(400);
            done();
        });
        executeMiddleware(() => {});
    });

    it('Should be unauthorized if oficinaUltimaConexion not equal to plaza norte', (done) => {
        const PASEO_AHUMADA = '0000';
        req.decoded.oficinaUltimaConexion = PASEO_AHUMADA;
        res.on('end', () => {
            expect(res.statusCode).to.be.equal(403);
            done();
        });
        executeMiddleware(() => {});
    });

    it('Should continue if oficinaUltimaConexion is equal to plaza norte', (done) => {
        executeMiddleware(() => {
            expect(res.statusCode).to.be.equal(200);
            done();
        });
    });
});