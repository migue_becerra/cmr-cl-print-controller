'use strict';

const chai = require('chai');
chai.use(require('chai-as-promised'));
const expect = chai.expect;
const mockery = require('mockery');


describe('DOCGEN.INVOKER:', () => {

    let invokerInput;

    beforeEach(() => {
        mockery.disable();
        mockery.deregisterAll();
        mockery.enable({
            useCleanCache: true,
            warnOnReplace: false,
            warnOnUnregistered: false
        });

        invokerInput = {
            decoded: {
                channelHeader: {
                    country: 'CL',
                    commerce: 'CMR',
                    channel: 'Mobile-APP'
                },
                requestId: '123456'
            },
            document: 'contrato'
        };
    });

    it('Should reject request without document', () => {
        delete invokerInput.document;
        return checkRejectedResponse(invokerInput, 'Malformed input');
    });

    it('Should reject request if document isn\'t part of domain', () => {
        invokerInput.document = '+chistoso';
        return checkRejectedResponse(invokerInput, 'Unknown document');
    });

    it('Should reject request that throw error', () => {
        mockery.registerMock('request', (opt, callback) => {
            callback(new Error('Generic error'));
        });
        return checkRejectedResponse(invokerInput, 'Failed to generate document');
    });

    const checkRejectedResponse = (input, rejectMessage) => {
        return chai.assert.isRejected(invokeInvoker(input), rejectMessage);
    };

    const invokeInvoker = input => {
        return require('./docgen.invoker')(input);
    };

    it('Should reject request that has response', () => {
        mockery.registerMock('request', (opt, callback) => {
            const response = undefined;
            const error = undefined;
            callback(error, response);
        });

        return checkRejectedResponse(invokerInput, 'Failed to generate document');
    });

    it('Should reject request that has no body', () => {
        mockery.registerMock('request', (opt, callback) => {
            const error = undefined;
            const body = undefined;
            const response = {
                statusCode: 200
            };
            callback(error, response, JSON.stringify(body));
        });
        return checkRejectedResponse(invokerInput, 'Failed to generate document');
    });

    it('Should reject not successful (200) response', () => {
        mockery.registerMock('request', (opt, callback) => {
            const error = undefined;
            const body = {
                some: 'body'
            };
            const response = {
                statusCode: 400
            };
            callback(error, response, JSON.stringify(body));
        });
        return checkRejectedResponse(invokerInput, 'Failed to generate document');
    });

    it('Should be fullfilled with successful (200) response', () => {
        const body = {
            some: 'body'
        };
        mockery.registerMock('request', (opt, callback) => {
            const error = undefined;
            const response = {
                statusCode: 200
            };
            callback(error, response, JSON.stringify(body));
        });
        const expectedBody = {
            some: 'body',
            document: 'contrato',
            decoded: {
                channelHeader: { country: 'CL', commerce: 'CMR', channel: 'Mobile-APP' },
                requestId: '123456'
            }
        };
        return expect(invokeInvoker(invokerInput)).to.eventually.deep.equal(expectedBody);
    });

    it('Should send GenerarContratoPDF doctype if contrato document is passed', () => {
        const body = {
            some: 'body'
        };
        mockery.registerMock('request', (opt, callback) => {
            const error = undefined;
            const response = {
                statusCode: 200
            };
            expect(opt.url).to.contains('GenerarContratoPDF');
            callback(error, response, JSON.stringify(body));
        });
        const expectedBody = {
            some: 'body',
            document: 'contrato',
            decoded: {
                channelHeader: { country: 'CL', commerce: 'CMR', channel: 'Mobile-APP' },
                requestId: '123456'
            }
        };
        return expect(invokeInvoker(invokerInput)).to.eventually.deep.equal(expectedBody);
    });

    it('Should send GenerarHojaResumenPDF doctype if resumen document is passed', () => {
        const body = {
            some: 'body'
        };
        mockery.registerMock('request', (opt, callback) => {
            const error = undefined;
            const response = {
                statusCode: 200
            };
            expect(opt.url).to.contains('GenerarHojaResumenPDF');
            callback(error, response, JSON.stringify(body));
        });
        invokerInput.document = 'resumen';

        const expectedBody = {
            some: 'body',
            document: 'resumen',
            decoded: {
                channelHeader: { country: 'CL', commerce: 'CMR', channel: 'Mobile-APP' },
                requestId: '123456'
            }
        };
        return expect(invokeInvoker(invokerInput)).to.eventually.deep.equal(expectedBody);
    });


});