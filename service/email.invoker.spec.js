'use strict';

const chai = require('chai');
chai.use(require('chai-as-promised'));
const expect = chai.expect;
const mockery = require('mockery');


describe('EMAIL.INVOKER:', () => {

    let invokerInput;

    beforeEach(() => {
        mockery.disable();
        mockery.deregisterAll();
        mockery.enable({
            useCleanCache: true,
            warnOnReplace: false,
            warnOnUnregistered: false
        });

        invokerInput = {
            decoded: {
                channelHeader: {
                    country: 'CL',
                    commerce: 'CMR',
                    channel: 'Mobile-APP'
                },
                requestId: '123456'
            },
            bodyHTML: '',
            attachment: [{
                name: 'contrato',
                pdfBase64: 'R1VJTExFUk1PIEFOICAgICAgICAgICAgICAgICAg'
            }]
        };
    });

    const checkRejectedResponse = (input, rejectMessage) => {
        return chai.assert.isRejected(invokeInvoker(input), rejectMessage);
    };

    const invokeInvoker = input => {
        return require('./email.invoker')(input);
    };

    it('Should reject request without response', () => {
        mockery.registerMock('request', (opt, callback) => {
            const response = undefined;
            const error = undefined;
            callback(error, response);
        });

        return checkRejectedResponse(invokerInput, 'Failed to send email');
    });

    it('Should reject request without body', () => {
        mockery.registerMock('request', (opt, callback) => {
            const error = undefined;
            const body = undefined;
            const response = {
                statusCode: 200
            };
            callback(error, response, body);
        });
        return checkRejectedResponse(invokerInput, 'Failed to send email');
    });

    it('Should reject not successful (200) response', () => {
        mockery.registerMock('request', (opt, callback) => {
            const error = undefined;
            const body = {
                some: 'body'
            };
            const response = {
                statusCode: 400
            };
            expect(opt.json.addressee).to.be.equal('PlazaNorteCMR@falabella.cl');
            expect(opt.json.subject).to.be.equal('Solicitud de apertura');
            expect(opt.json.bodyHTML).to.be.equal('Estimados,Por favor imprimir contrato de Apertura,Slds,Ejecutiva de Captación');
            callback(error, response, body);
        });
        return checkRejectedResponse(invokerInput, 'Failed to send email');
    });

    it('Should be fullfilled with successful (200) response', () => {
        const body = {
            some: 'body'
        };
        mockery.registerMock('request', (opt, callback) => {
            const error = undefined;
            const response = {
                statusCode: 200
            };
            callback(error, response, body);
        });
        return expect(invokeInvoker(invokerInput)).to.eventually.deep.equal(body);
    });
});