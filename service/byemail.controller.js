'use strict';

const docgenInvoker = require('./docgen.invoker');
const emailInvoker = require('./email.invoker');
const lib = require('../lib');
const logger = lib.logger;

module.exports = (req, res) => {
    req.document = req.body.documents[0];
    docgenInvoker(req)
        .then(emailInvoker)
        .then(result => {
            logger.info('BYEMAIL.CONTROLLER: got document ' + req.body.documents);
            res.json({
                code: 'SUCCESS'
            });
        })
        .catch(err => {
            logger.error('BYEMAIL.CONTROLLER.ERROR: ' + lib.getStackTrace(err));
            res.status(500).json({
                code: 'COMMUNICATION_FAILURE',
                message: err.message
            });
        });
};