'use strict';

const lib = require('../lib/index');
const _ = require('lodash');
const logger = lib.logger;

const PLAZA_NORTE = '1737';

const verify = (req, res, next) => {
    logger.info('VERIFY.PRINTING.AUTHORIZATION.MIDDLEWARE: Starting Authorization Verification');
    if (!_.has(req, 'decoded.oficinaUltimaConexion')) {
        logger.error('VERIFY.PRINTING.AUTHORIZATION.MIDDLEWARE: decoded or oficinaUltimaConexion is missing');
        res.status(400).json({
            code: 'BAD_REQUEST',
            message: 'decoded or oficinaUltimaConexion is missing'
        });
        res.end();
    } else if (req.decoded.oficinaUltimaConexion === PLAZA_NORTE) {
        next();
    } else {
        logger.error('VERIFY.PRINTING.AUTHORIZATION.MIDDLEWARE: Branch is not authorized');
        res.status(403).json({
            code: 'NOT_AUTHORIZED',
            message: 'Branch is not authorized'
        });
    }
};

module.exports = verify;