'use strict';

const chai = require('chai');
const expect = chai.expect;
const httpMocks = require('node-mocks-http');
const mockery = require('mockery');

let req;
let res;

describe('REQUEST.ID.VERIFY.MIDDLEWARE:', () => {
    beforeEach(() => {
        req = httpMocks.createRequest();
        res = httpMocks.createResponse({
            eventEmitter: require('events').EventEmitter
        });
        req.decoded = {
            run: '19'
        };
        req.body = {
            requestId: 'cuchufleta'
        };
        mockery.enable({
            warnOnReplace: false,
            warnOnUnregistered: false,
            useCleanCache: true
        });
    });

    afterEach(() => {
        mockery.disable();
        mockery.deregisterAll();
    });

    const executeMiddleware = (next) => {
        const middleware = require('./request.id.verify.middleware');
        middleware(req, res, next);
    };

    it('Should be bad request if no requestId sent', (done) => {
        delete req.body.requestId;
        res.on('end', () => {
            expect(res.statusCode).to.be.equal(400);
            done();
        });
        executeMiddleware(() => {});
    });

    it('Should be unauthorized if requestId cant be verified', (done) => {
        mockery.registerMock('jsonwebtoken', {
            verify: (payload, secret, callback) => {
                callback(new Error('Mocked error verifying payload'));
            }
        });
        res.on('end', () => {
            expect(res.statusCode).to.be.equal(403);
            done();
        });
        executeMiddleware(() => {});
    });

    it('Should be unauthorized if promotor run is not present', (done) => {
        delete req.decoded.run;
        res.on('end', () => {
            expect(res.statusCode).to.be.equal(400);
            done();
        });
        executeMiddleware(() => {});
    });

    it('Should continue if requestId is verified', (done) => {
        mockery.registerMock('jsonwebtoken', {
            verify: (payload, secret, callback) => {
                callback(null, { id: '123' });
            }
        });
        executeMiddleware(() => {
            expect(req.decoded.requestId).to.be.equal('123');
            done();
        });
    });


});