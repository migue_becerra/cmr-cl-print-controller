Feature: To support handling conectivity error
As API Client
In order to check the connection with it providers
I want to check the communication

Background:
  Given I have app up and running
  And providers not answer

  Scenario Outline:
  When I post to "<serviceURL>" with "<solicitudID>" and "<documentType>" correctily
  Then I gott "<apiCode>" 
  And check "<httpStatus>"

  Examples:
| serviceURL                                     | solicitudID | documentType | apiCode | httpStatus |
#Fail Api Obtener#
| http://localhost:3006/api/cmr/print/v1/byemail | 1500000     | contrato     | FAILURE  | 500       |
# Fail docgen #
| http://localhost:3006/api/cmr/print/v1/byemail | 1400000     | contrato     | FAILURE  | 500       |
# Fail email #
| http://localhost:3006/api/cmr/print/v1/byemail | 1300000     | contrato     | FAILURE  | 500       |
