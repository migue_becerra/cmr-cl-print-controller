Feature: Check service is up and running

  Background:
    Given I have app up and running

  Scenario Outline: Display correctly service is running
    When I get "<serviceUrl>"
    Then I can see title "<serviceTitle>"

    Examples:
      | serviceUrl | serviceTitle |
      | http://localhost:3006/api/cmr/print/v1/health | Service is up and running |
