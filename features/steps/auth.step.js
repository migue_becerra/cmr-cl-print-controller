const request = require('request');
const assert = require('cucumber-assert');

module.exports = function() {

  this.Given(/^has a recruteur logged in$/, function(callback) {
    callback();
  });

  this.Given(/^providers not answer$/, function(callback) {
    callback();
  });

  let token;
  this.When(/^I am trying to print from branch "([^"]*)", "([^"]*)"$/, function(code, name, callback) {
    token = code;
    callback();
  });

  let globalResponse;

  this.When(/^I get the Service URL "([^"]*)"$/, function(serviceUrl, callback) {
    request.get({
      url: serviceUrl,
      headers: {
        'x-access-token': token
      }
    }, (error, response, body) => {
      if (error) {
        return callback(error);
      }
      return callback(null, globalResponse = response);
    });
  });

  this.When(/^the API answer "([^"]*)"$/, function(answer, callback) {
    const body = JSON.parse(globalResponse.body);
    assert.equal(body.code, answer, callback, 'El cuerpo de la response deberia contener ' + answer);
  });

};
