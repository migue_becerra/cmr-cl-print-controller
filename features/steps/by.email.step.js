const request = require('request');
const assert = require('cucumber-assert');
const lib = require('../../lib');
const logger = lib.logger;

module.exports = function() {

    let globalResponse;
    const postToService = (serviceUrl, requestId, requestedDocuments, callback) => {
        const documents = requestedDocuments.split(',');
        request.post({
            url: serviceUrl,
            headers: {
                'x-access-token': '1737'
            },
            json: {
                requestId: require('jsonwebtoken').sign({ id: requestId }, '19', { expiresIn: 86400 }),
                documents: documents
            }
        }, (error, response, body) => {
            if (error) {
                logger.error('BYEMAIL.STEP ' + lib.getStackTrace(error));
                return callback(error);
            }
            return callback(null, globalResponse = response);
        });
    };

    this.When(/^I post to "([^"]*)" with "([^"]*)" and "([^"]*)" with different errors$/, postToService);
    this.When(/^I post to "([^"]*)" with "([^"]*)" and "([^"]*)" correctly$/, postToService);

    this.When(/^I got "([^"]*)"$/, function(answer, callback) {
        logger.info('BYEMAIL.STEP ' + JSON.stringify(globalResponse.body));
        assert.equal(globalResponse.body.code, answer, callback, 'El codigo de respuesta deberia ser ' + answer);
    });

    const checkHttpStatus = (httpStatus, callback) => {
        assert.equal(globalResponse.statusCode, httpStatus, callback, 'El http status de la respuesta deberia ser ' + httpStatus);
    };

    this.When(/^"([^"]*)" in an error notification for problems with the validations$/, checkHttpStatus);
    this.When(/^"([^"]*)" from a success answer$/, checkHttpStatus);
    this.When(/^check "([^"]*)"$/, checkHttpStatus);

};