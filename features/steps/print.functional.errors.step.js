const request = require('request');
const assert = require('cucumber-assert');
const lib = require('../../lib');
const logger = lib.logger;

module.exports = function() {

    let globalResponse;
    const postToService = (serviceUrl, requestId, requestedDocuments, branchCode, callback) => {
        const documents = requestedDocuments.split(',');
        request.post({
            url: serviceUrl,
            headers: {
                'x-access-token': branchCode
            },
            json: {
                requestId: require('jsonwebtoken').sign({ id: requestId }, '19', { expiresIn: 86400 }),
                documents: documents
            }
        }, (error, response, body) => {
            if (error) {
                logger.error('PRINT.FUNCTIONAL.ERRORS.STEP ' + lib.getStackTrace(error));
                return callback(error);
            }
            return callback(null, globalResponse = response);
        });
    };

    this.When(/^I post to "([^"]*)" with "([^"]*)" and "([^"]*)" from "([^"]*)" which is unauthorized$/, postToService);

    this.When(/^I got an "([^"]*)" response$/, function(answer, callback) {
        logger.info('PRINT.FUNCTIONAL.ERRORS.STEP ' + JSON.stringify(globalResponse.body));
        assert.equal(globalResponse.body.code, answer, callback, 'El codigo de respuesta deberia ser ' + answer);
    });

    const checkHttpStatus = (httpStatus, callback) => {
        assert.equal(globalResponse.statusCode, httpStatus, callback, 'El http status de la respuesta deberia ser ' + httpStatus);
    };

    this.When(/^"([^"]*)" reporting a functional error$/, checkHttpStatus);

};