const request = require('request');
const assert = require('cucumber-assert');

module.exports = function() {

  let globalResponse;

  this.When(/^I get "([^"]*)"$/, function(stringInDoubleQuotes, callback) {
    request.get(stringInDoubleQuotes, (error, response, body) => {
      if (error) {
        return callback(error);
      }
      return callback(null, globalResponse = response);
    });
  });

  this.Then(/^I can see title "([^"]*)"$/, function(stringInDoubleQuotes, callback) {
    assert.equal(globalResponse.body.includes(stringInDoubleQuotes), true,
      callback, 'El cuerpo de la response deberia contener ' + stringInDoubleQuotes);
  });

};
