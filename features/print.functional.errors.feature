Feature: Reporting Functional Errors
  As a Recruteir
  In order to open a credit card for a client
  I want to be informed for a functional error

  Background:
    Given I have app up and running
    And has a recruteur logged in

  Scenario Outline: Printing documents from a unauthorized branch
    When I post to "<serviceURL>" with "<solicitudID>" and "<documentType>" from "<branchCode>" which is unauthorized
    Then I got an "<apiCode>" response
    And "<httpStatus>" reporting a functional error

    Examples:
      | serviceURL                                     | solicitudID | documentType | branchCode | apiCode        | httpStatus |
      | http://localhost:3006/api/cmr/print/v1/byemail | 12345678    | contrato     | 0000       | NOT_AUTHORIZED | 403        |