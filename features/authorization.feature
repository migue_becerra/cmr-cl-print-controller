Feature: Check the authorization to send the documentation by email
  As a recruteur
  In order to open a credit card for a client
  I want to know if I have authorization to send documentation

  Background:
    Given I have app up and running
    And has a recruteur logged in

  Scenario Outline: check for a recruteur from a branch different from Plaza Norte (1737)
    When I am trying to print from branch "<branchID>", "<branchName>"
    And I get the Service URL "<serviceURL>"
    Then the API answer "<responseCode>"

    Examples:
      | branchName | branchID | serviceURL | responseCode |
      | Plaza Norte | 1737 | http://localhost:3006/api/cmr/print/v1/auth | AUTHORIZED |
      | Ahumada | 0000 | http://localhost:3006/api/cmr/print/v1/auth | NOT_AUTHORIZED |
      | Nva Lyon | 0070 | http://localhost:3006/api/cmr/print/v1/auth | NOT_AUTHORIZED |
      #cuando no viene el token
      |  |  | http://localhost:3006/api/cmr/print/v1/auth | 403 |
