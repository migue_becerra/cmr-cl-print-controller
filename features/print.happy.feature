Feature: Send the list of documents you need to generate
  As a Recruteir
  In order to open a credit card for a client
  I want to know if I have authorization to send documentation

  Background:
    Given I have app up and running
    And has a recruteur logged in

Scenario Outline: Printing documents
    When I post to "<serviceURL>" with "<solicitudID>" and "<documentType>" correctly
    Then I got "<apiCode>"
    And "<httpStatus>" from a success answer

    Examples:
| serviceURL                                     | solicitudID | documentType | apiCode | httpStatus |
| http://localhost:3006/api/cmr/print/v1/byemail | 1525038153  | contrato     | SUCCESS  | 200  |
| http://localhost:3006/api/cmr/print/v1/byemail | 1525038     | contrato     | SUCCESS  | 200  |
