Feature: Send the list of documents you need to generate
  As a Recruteir
  In order to open a credit card for a client
  I want to generate formal documentation and send it by email

  Background:
    Given I have app up and running
    And has a recruteur logged in

  Scenario Outline: Printing documents
    When I post to "<serviceURL>" with "<solicitudID>" and "<documentType>" with different errors
    Then I got "<apiCode>"
    And "<httpStatus>" in an error notification for problems with the validations

    Examples:
      | serviceURL                                     | solicitudID  | documentType                                         | apiCode     | httpStatus |
      | http://localhost:3006/api/cmr/print/v1/byemail | 12347·$%89   | contrato                                             | BAD_REQUEST | 400        |
      | http://localhost:3006/api/cmr/print/v1/byemail | 1525038%     | contrato                                             | BAD_REQUEST | 400        |
      | http://localhost:3006/api/cmr/print/v1/byemail | chao         | contrato                                             | BAD_REQUEST | 400        |
      | http://localhost:3006/api/cmr/print/v1/byemail | 3hola2       | contrato                                             | BAD_REQUEST | 400        |
      | http://localhost:3006/api/cmr/print/v1/byemail | hola34535    | contrato                                             | BAD_REQUEST | 400        |
      | http://localhost:3006/api/cmr/print/v1/byemail | 1596hol      | contrato                                             | BAD_REQUEST | 400        |
      | http://localhost:3006/api/cmr/print/v1/byemail | 1515  321    | contrato                                             | BAD_REQUEST | 400        |
      | http://localhost:3006/api/cmr/print/v1/byemail |              | contrato                                             | BAD_REQUEST | 400        |
      | http://localhost:3006/api/cmr/print/v1/byemail | 152503878911 | contrato                                             | BAD_REQUEST | 400        |
      | http://localhost:3006/api/cmr/print/v1/byemail | 1525038      |                                                      | BAD_REQUEST | 400        |
      | http://localhost:3006/api/cmr/print/v1/byemail | 1525038      | contrao                                              | BAD_REQUEST | 400        |
      | http://localhost:3006/api/cmr/print/v1/byemail | 1525038      | contrto                                              | BAD_REQUEST | 400        |
      | http://localhost:3006/api/cmr/print/v1/byemail | 123456789    | con8trato                                            | BAD_REQUEST | 400        |
      | http://localhost:3006/api/cmr/print/v1/byemail | 1525038      | &contrato.                                          | BAD_REQUEST | 400        |
      | http://localhost:3006/api/cmr/print/v1/byemail | 1525038      | contrto                                              | BAD_REQUEST | 400        |
      | http://localhost:3006/api/cmr/print/v1/byemail | 1525038      | contrato,contrato                                    | BAD_REQUEST | 400        |
      | http://localhost:3006/api/cmr/print/v1/byemail | 1525038      | contrato,contrato,contrato,contrato,contrato         | BAD_REQUEST | 400        |
      | http://localhost:3006/api/cmr/print/v1/byemail | 1525038789   | contrato,contrato                                    | BAD_REQUEST | 400        |
      | http://localhost:3006/api/cmr/print/v1/byemail | 1525038789   | contrato,contrato,contrato,contrato,contrato         | BAD_REQUEST | 400        |
